<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();
        $this->token = $this->getBearerToken();
    }

    public function testLoginIfCheckForEmailField()
    {
        $user = factory(User::class)->make();

        $this->json('POST', '/auth/login', [
            'password' => $user->password
        ])->seeJson([
            "email" => ["The email field is required."],
        ]);
    }

    public function testLoginIfCheckForPasswordField()
    {
        $user = factory(User::class)->make();

        $this->json('POST', '/auth/login', [
            'email' => $user->email
        ])->seeJson([
            "password" => ["The password field is required."],
        ]);
    }

    public function testLoginWithInvalidEmail()
    {
        $user = factory(User::class)->make();

        $this->json('POST', '/auth/login', [
            'email' => $user->email,
            'password' => $user->password
        ])->seeJson([
            "status" => 404,
        ]);
    }

    public function testLoginWithInvalidPassword()
    {
        $user = factory(User::class)->create();

        $this->json('POST', '/auth/login', [
            'email' => $user->email,
            'password' => 'invalid_password'
        ])->seeJson([
            "status" => 400,
        ]);;
    }

    public function testLoginWithValidUser()
    {
        $user = factory(User::class)->create();

        $this->json('POST', '/auth/login', [
            'email' => $user->email,
            'password' => '123456'
        ])->seeJsonStructure(['token']);
    }
}
