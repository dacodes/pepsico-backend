<?php

use App\Models\Gif;
use Illuminate\Http\UploadedFile;
use Laravel\Lumen\Testing\DatabaseTransactions;

class GifsTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();
        $this->token = $this->getBearerToken();
    }

    public function testRegisterIfCheckForNameField()
    {
        $this->json('POST', '/gifs', [])
            ->seeJson([
                "name" => ["The name field is required."],
            ]);
    }

    public function testRegisterIfCheckForGifField()
    {
        $gif = factory(Gif::class)->make();

        $this->json('POST', '/gifs', [
            'name' => $gif->name,
        ])->seeJson([
            "gif" => ["The gif field is required."],
        ]);
    }

    // public function testRegisterWithValidGif()
    // {
    //     $gif = factory(Gif::class)->make();

    //     $this->json('POST', '/gifs', [
    //         'name' => $gif->name,
    //         'gif' => UploadedFile::fake()->image('image.jpg'),
    //     ], [
    //         'Authorization' => $this->token
    //     ]);

    //     dd($this->response->getContent());

    //     $this->assertEquals(201, $this->response->status());
    // }

    public function testUpdateIfCheckForEmptyNameField()
    {
        $gif = factory(Gif::class)->create();

        $this->json('PUT', "/gifs/$gif->id", [], [
            'Authorization' => $this->token
        ])
            ->seeJson([
                "name" => ["The name field is required."],
            ]);
    }

    public function testUpdateIfCheckForEmptyGifField()
    {
        $gif = factory(Gif::class)->create();

        $this->json('PUT', "/gifs/$gif->id", [
            'name' => $gif->name
        ], [
            'Authorization' => $this->token
        ])->seeJson([
            "accepted" => ["The accepted field is required."],
        ]);
    }

    public function testUpdateIfCheckForAcceptedField()
    {
        $gif = factory(Gif::class)->create();

        $this->json('PUT', "/gifs/$gif->id", [
            'name' => $gif->name,
        ], [
            'Authorization' => $this->token
        ])->seeJson([
            "accepted" => ["The accepted field is required."],
        ]);
    }

    public function testUpdateIfCheckForNonBooleanAcceptedField()
    {
        $gif = factory(Gif::class)->create();

        $this->json('PUT', "/gifs/$gif->id", [
            'name' => $gif->name,
            'accepted' => "invalid_boolean"
        ], [
            'Authorization' => $this->token
        ])->seeJson([
            "accepted" => ["The accepted field must be true or false."],
        ]);
    }

    public function testAllGifs()
    {
        $this->json('GET', "/gifs", []);

        $this->assertEquals(200, $this->response->status());
    }

    public function testGetGifWithInvalidID()
    {
        $this->json('GET', "/gifs/0", []);

        $this->assertEquals(404, $this->response->status());
    }

    public function testGetGifWithValidID()
    {
        $gif = factory(Gif::class)->create();

        $this->json('GET', "/gifs/$gif->id", []);

        $this->assertEquals(200, $this->response->status());
    }
}
