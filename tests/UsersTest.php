<?php

use App\Models\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UsersTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();
        $this->token = $this->getBearerToken();
    }

    public function testRegisterIfCheckForNameField()
    {
        $this->json('POST', '/users', [], [
            'Authorization' => $this->token
        ])
            ->seeJson([
                "name" => ["The name field is required."],
            ]);
    }

    public function testRegisterIfCheckForEmailField()
    {
        $user = factory(User::class)->make();

        $this->json('POST', '/users', [
            'name' => $user->name,
            'last_name' => $user->last_name
        ], [
            'Authorization' => $this->token
        ])->seeJson([
            "email" => ["The email field is required."],
        ]);
    }

    public function testRegisterIfCheckForEmailFieldFormat()
    {
        $user = factory(User::class)->make();

        $this->json('POST', '/users', [
            'name' => $user->name,
            'email' => 'invalid_email',
        ], [
            'Authorization' => $this->token
        ])->seeJson([
            "email" => ["The email must be a valid email address."],
        ]);
    }

    public function testRegisterIfCheckForPasswordField()
    {
        $user = factory(User::class)->make();

        $this->json('POST', '/users', [
            'name' => $user->name,
            'email' => $user->email,
        ], [
            'Authorization' => $this->token
        ])->seeJson([
            "password" => ["The password field is required."],
        ]);
    }

    public function testRegisterWithExistingEmail()
    {
        $user = factory('App\Models\User')->create();

        $this->json('POST', '/users', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
        ], [
            'Authorization' => $this->token
        ])->seeJson([
            "status" => 400,
        ]);
    }

    public function testRegisterWithValidUser()
    {
        $user = factory('App\Models\User')->make();

        $this->json('POST', '/users', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
        ], [
            'Authorization' => $this->token
        ]);

        $this->assertEquals(201, $this->response->status());
    }

    public function testAllUsers()
    {
        $this->json('GET', "/users", []);

        $this->assertEquals(200, $this->response->status());
    }
}
