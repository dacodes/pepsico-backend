<?php

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__. '/../bootstrap/app.php';
    }

    public function getBearerToken(): string
    {   
        $authService = $this->app->make('App\Components\Auth\AuthService');
        $user = factory('App\Models\User')->create();
        return 'Bearer ' . $authService->jwt($user);
    }
}
