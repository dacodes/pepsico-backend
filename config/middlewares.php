<?php

/**
 * Middleware configuration
 */

return [
    'routes' => [
        'auth' => \App\Middlewares\JwtMiddleware::class,
    ],
    'global' => [
        App\Middlewares\CorsMiddleware::class
    ]
];
