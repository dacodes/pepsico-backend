<?php

/**
 * Logger configuration
 */
return [
  'name' => 'lumen',
  'handlers' => [
    'debug' => [
      'path' => log_path() . '/debug.log',
      'level' => \Monolog\Logger::DEBUG
    ],
    'info' => [
      'path' => log_path() . '/info.log',
      'level' => \Monolog\Logger::INFO
    ],
    'warning' => [
      'path' => log_path() . '/warning.log',
      'level' => \Monolog\Logger::WARNING
    ],
    'error' => [
      'path' => log_path() . '/error.log',
      'level' => \Monolog\Logger::ERROR
    ]
  ]
];
