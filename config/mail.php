<?php

/**
 * Mail configuration
 */
return [
    'host' => getenv('MAIL_HOST'),
    'port' => getenv('MAIL_PORT'),
    'from' => getenv('MAIL_FROM'),
    'encryption' => getenv('MAIL_ENCRYPTION'),
    'username' => getenv('MAIL_USER'),
    'password' => getenv('MAIL_PASSWORD')
];
