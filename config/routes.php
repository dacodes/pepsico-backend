<?php

return [
    \App\Components\Base\Router::class,
    \App\Components\Auth\AuthRouter::class,
    \App\Components\Users\UsersRouter::class,
    \App\Components\Gifs\GifsRouter::class,
];
