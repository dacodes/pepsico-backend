<?php

/**
 * Providers configuration
 */
return [
    \App\Providers\AppServiceProvider::class,
    \App\Providers\AuthServiceProvider::class,
    \App\Providers\EventServiceProvider::class,
    \App\Providers\MailServiceProvider::class,
    \App\Providers\FlySystemServiceProvider::class
];
