# Pepsi Backend
This is the backend for Pepsi Test

# Requirements

 - [PHP](https://www.php.net/manual/en/install.php) >= 7.0 
 - PHP Extensions `OpenSSL` `PDO` and `Mbstring`
 - [Composer](https://getcomposer.org/)

# Installation

Clone the repository 

    git clone https://bitbucket.org/dacodes/pepsico-backend

Install dependencies

    composer install

> For **production** add this flags `--optimize-autoloader --no-dev` to composer install

# Configuration

## Environment
- Rename file `example.env` to `.env`
- Add your database configuration to the `DB_` variables

## Database
- Create the database with collation `utf8mb4` 
- Run command `php artisan migrate` for create all the database tables

> Run command `php artisan db:seed` for seeding the database (This command only works when run composer without production flags)

# Execute unit tests

Run php unit test

    vendor/bin/phpunit

# Run

Run command `php -S 0.0.0.0:80 -t public`

> For **Production** only copy the files project to the production server.
