<?php

require __DIR__ . '/../vendor/autoload.php';

/**
 * Bootstrap teh enviroment
 */
(new \Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
    dirname(__DIR__))
)->bootstrap();


/**
 * Instantiate the app
 */
$app = new \App\Kernel\App(dirname(__DIR__));

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

return $app;
