<?php

use App\Models\Gif;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => app('hash')->make('123456'),
    ];
});

$factory->define(Gif::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'url' => 'https://media.giphy.com/media/de9SDw6PGRsubN1o3X/giphy.gif',
    ];
});
