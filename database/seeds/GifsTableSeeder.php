<?php

use Illuminate\Database\Seeder;

class GifsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create 10 gifs using the gifs factory
        factory(App\Models\Gif::class, 10)->create();
    }
}
