<?php

namespace App\Components\Users;

use App\Components\Auth\AuthRepository;
use App\Exceptions\ApiException;

/**
 * Service for users controller
 */
class UsersService
{
    /**
     * Service constructor
     *
     * @param AuthRepository $authRepository
     */
    function __construct(
        AuthRepository $authRepository,
        UsersRepository $usersRepository
    ) {
        $this->authRepository = $authRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Register a new user
     *
     * @param  string $name
     * @param  string $email
     * @param  string $password
     */
    function register(string $name, string $email, string $password)
    {
        $user = $this->authRepository->getByEmail($email);

        if ($user != null) {
            throw new ApiException("The email already exists", 400);
        }

        $passwordHashed = app('hash')->make($password);

        return $this->usersRepository->register(
            $name,
            $email,
            $passwordHashed
        );
    }

    /**
     * Get a list of users
     *
     * @return array
     */
    function all(): array
    {
        return $this->usersRepository->all()->toArray();
    }
}
