<?php

namespace App\Components\Users;

use App\Components\Base\Controller;
use Illuminate\Http\Request;

/**
 * Users controller
 */
class UsersController extends Controller
{
    /**
     * Users controller constructor
     *
     * @param UsersService $service
     */
    function __construct(UsersService $service)
    {
        $this->service = $service;
    }

    /**
     * Register a new user.
     *
     * @param  Request     $request
     * @return JSON Response
     */
    function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = $this->service->register(
            $request->input('name'),
            $request->input('email'),
            $request->input('password')
        );

        return response()->json($user, 201);
    }

    /**
     * Get all user
     *
     * @return JSON Response
     */
    function all()
    {
        $users = $this->service->all();

        return response()->json($users, 200);
    }
}
