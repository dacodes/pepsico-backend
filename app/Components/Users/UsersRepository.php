<?php

namespace App\Components\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Users repository
 */
class UsersRepository
{
    /**
     * Get all users
     * @return array
     */
    function all(): Collection
    {
        return User::all();
    }

    /**
     * Register a new user in the database
     *
     * @param  string $name
     * @param  string $email
     * @param  string $password
     */
    function register(string $name, string $email, string $password): User
    {
        $user = new User();

        $user->name = $name;
        $user->email = $email;
        $user->password = $password;

        $user->save();

        return $user;
    }
}
