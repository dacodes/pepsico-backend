<?php

namespace App\Components\Users;

use Laravel\Lumen\Routing\Router;

/**
 * Users router
 */
class UsersRouter
{
    function __construct(Router $router)
    {
        $router->group([
            'namespace' => 'App\Components\Users',
            'prefix' => 'users'
        ], function () use ($router) {
            $router->post('/', 'UsersController@register');

            $router->get('/', 'UsersController@all');
        });
    }
}
