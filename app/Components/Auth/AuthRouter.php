<?php

namespace App\Components\Auth;

use Laravel\Lumen\Routing\Router;

class AuthRouter
{
    function __construct(Router $router)
    {
        $router->group([
            'namespace' => 'App\Components\Auth',
            'prefix' => 'auth'
        ], function () use ($router) {
            $router->post('/login', 'AuthController@login');
        });
    }
}
