<?php

namespace App\Components\Auth;

use App\Models\User;

/**
 * Auth repository
 */
class AuthRepository
{
    /**
     * Get user by email
     *
     * @param  string $email
     * @return User
     */
    function getByEmail(string $email): ?User
    {
        return User::where('email', $email)->first();
    }
}
