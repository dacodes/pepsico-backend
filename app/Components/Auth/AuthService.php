<?php

namespace App\Components\Auth;

use App\Exceptions\ApiException;
use App\Helpers\MailHelper;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

/**
 * Service for auth controller
 */
class AuthService
{
    /**
     * Service constructor
     *
     * @param AuthRepository $repository
     */
    function __construct(AuthRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Verify the user credentials
     *
     * @param  string $email
     * @param  string $password
     * @return string
     */
    function login(string $email, string $password): User
    {
        $user = $this->repository->getByEmail($email);

        if ($user == null) {
            throw new ApiException("The user does not exists", 404);
        }

        if (!Hash::check($password, $user->password)) {
            throw new ApiException("Email or password is wrong", 400);
        }

        return $user;
    }

    /**
     * Create a new token from user.
     *
     * @param  User   $user
     * @return string
     */
    function jwt(User $user): string
    {
        $payload = [
            'iss' => "jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            // 'exp' => time() + 60*60 // Expiration time
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }
}
