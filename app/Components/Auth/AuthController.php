<?php

namespace App\Components\Auth;

use App\Components\Base\Controller;
use App\Helpers\MailHelper;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Auth controller
 */
class AuthController extends Controller
{
    /**
     * Auth controller constructor
     *
     * @param AuthService $service
     */
    function __construct(AuthService $service)
    {
        $this->service = $service;
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param  Request     $request
     * @return JSON Response
     */
    function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = $this->service->login(
            $request->input('email'),
            $request->input('password')
        );

        return response()->json(
            array_merge($user->toArray(), ['token' => $this->service->jwt($user)]),
            200
        );
    }
}
