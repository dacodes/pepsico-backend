<?php

namespace App\Components\Gifs;

use Illuminate\Database\Eloquent\Collection;
use App\Models\Gif;

/**
 * Gifs repository
 */
class GifsRepository
{
    /**
     * Get all Gifs
     * @return array
     */
    function all(): Collection
    {
        return Gif::all();
    }

    /**
     * Get all Gifs
     * @return array
     */
    function getAccepted(): Collection
    {
        return Gif::where('accepted', 1)->get();
    }

    /**
     * Get gif by id
     *
     * @param  int    $id
     * @return Gif
     */
    function get(int $id): ?Gif
    {
        return Gif::where('id', '=', $id)->first();
    }

    /**
     * Register a new gif in the database
     *
     * @param  string $name
     * @param  string $url
     */
    function register(string $name, string $url): Gif
    {
        $gif = new Gif();

        $gif->name = $name;
        $gif->url = $url;

        $gif->save();

        return $gif;
    }

    /**
     * Update a existing gif in the database
     *
     * @param  string $name
     * @param  bool $accepted
     */
    function update(Gif $gif, array $data): Gif
    {
        $gif->name = $data['name'];
        $gif->accepted = $data['accepted'];

        $gif->save();

        return $gif;
    }
}
