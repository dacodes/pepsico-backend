<?php

namespace App\Components\Gifs;

use Laravel\Lumen\Routing\Router;

/**
 * Gifs router
 */
class GifsRouter
{
    function __construct(Router $router)
    {
        $router->group([
            'namespace' => 'App\Components\Gifs',
            'prefix' => 'gifs'
        ], function () use ($router) {
            $router->post('/', 'GifsController@register');

            $router->get('/', 'GifsController@all');

            $router->get('/{id}', 'GifsController@get');

            $router->group(['middleware' => 'auth'], function () use ($router) {
                $router->put('/{id}', 'GifsController@update');
            });
        });
    }
}
