<?php

namespace App\Components\Gifs;

use App\Components\Base\Controller;
use App\Helpers\UriHelper;
use Illuminate\Http\Request;
use League\Flysystem\Filesystem;

/**
 * Gifs controller
 */
class GifsController extends Controller
{
    private $gifsService;

    /**
     * Gifs controller constructor
     *
     * @param GifsService $gifsService
     */
    function __construct(GifsService $gifsService)
    {
        $this->gifsService = $gifsService;
    }

    /**
     * Register a new gif.
     *
     * @param  Request     $request
     * @return JSON Response
     */
    function register(Request $request)
    {
        $this->validate($request, [
            'gif' => 'required|image',
            'name' => 'required|max:100',
        ]);

        $gif = $this->gifsService->register(
            $request->input('name'),
            $request->file('gif')
        );

        return response()->json($gif, 201);
    }

    /**
     * Get all accepted gifs
     * @param  Request $request
     * @return JSON Response
     */
    function all(Request $request)
    {
        $gifs = $this->gifsService->all($request->get('filter'));

        return response()->json($gifs, 200);
    }

    /**
     * Get gif by ID
     *
     * @param  int $id
     * @return JSON Response
     */
    function get(int $id)
    {
        $gif = $this->gifsService->get($id);

        return response()->json($gif, 200);
    }

    /**
     * Update current gif
     *
     * @param  Request  $request
     * @param  int      $id
     * @return JSON
     */
    function update(Request $request, int $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'accepted' => 'required|boolean'
        ]);

        $gif = $this->gifsService->update($id, $request->all());

        return response()->json($gif, 200);
    }
}
