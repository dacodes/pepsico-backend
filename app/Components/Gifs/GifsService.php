<?php

namespace App\Components\Gifs;

use App\Exceptions\ApiException;
use App\Models\Gif;
use Illuminate\Http\UploadedFile;
use League\Flysystem\Filesystem;

/**
 * Service for gifs controller
 */
class GifsService
{
    private $gifsRepository;
    private $filesystem;

    /**
     * Service constructor
     *
     * @param GifsRepository $gifsRepository
     */
    function __construct(
        GifsRepository $gifsRepository,
        Filesystem $filesystem
    ) {
        $this->gifsRepository = $gifsRepository;
        $this->filesystem = $filesystem;
    }

    /**
     * Register a new gif
     *
     * @param  string $name
     * @param  UploadedFile $file
     */
    function register(string $name, UploadedFile $file): Gif
    {
        $fileName = $file->getClientOriginalName();
        $path = "/media/gifs/{$fileName}";
        $content = file_get_contents($file);

        $response = $this->filesystem->put($path, $content);

        if (!$response) {
            throw new ApiException('Error trying to save gif', 500);
        }

        return $this->gifsRepository->register(
            $name,
            url('/') . "/media/gifs/$fileName"
        );
    }

    /**
     * Get a list of gifs
     *
     * @return array
     */
    function all(?string $filter): array
    {
        if ($filter != null) {
            return $this->gifsRepository->getAccepted()->toArray();
        } else {
            return $this->gifsRepository->all()->toArray();
        }
    }

    /**
     * Get a list of gifs
     * @param  int $id
     * @return Gif
     */
    function get(int $id): Gif
    {
        $gif = $this->gifsRepository->get($id);

        if ($gif == null) {
            throw new ApiException('The gif does not exists', 404);
        }

        return $gif;
    }

    /**
     * Update current gif
     *
     * @param  int    $id
     * @param  array  $data
     */
    function update(int $id, array $data): Gif
    {
        $gif = $this->gifsRepository->get($id);

        if ($gif == null) {
            throw new ApiException('The gif does not exists', 404);
        }

        return $this->gifsRepository->update($gif, $data);
    }
}
