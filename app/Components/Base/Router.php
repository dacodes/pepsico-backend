<?php

namespace App\Components\Base;

use Laravel\Lumen\Routing\Router as BaseRouter;

class Router
{
  function __construct(BaseRouter $router)
  {

    $router->get('/', function () use ($router) {
      return view('index');
    });
  }
}
