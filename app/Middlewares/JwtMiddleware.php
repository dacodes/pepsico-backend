<?php

namespace App\Middlewares;

use App\Exceptions\AuthorizationException;
use App\Models\User;
use Closure;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();

        if (!$token) {
            throw new AuthorizationException("Token not provided.", 401);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch (ExpiredException $e) {
            throw new AuthorizationException("Provided token is expired.", 400);
        } catch (Exception $e) {
            throw new AuthorizationException("An error while decoding token.", 400);
        }

        $user = User::find($credentials->sub);

        $request->auth = $user;

        return $next($request);
    }
}
