<?php

namespace App\Exceptions;

class ApiException extends \Exception {

    public function __construct(string $message, int $status) {
        parent::__construct($message, $status);
    }
}
