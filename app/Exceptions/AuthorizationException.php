<?php

namespace App\Exceptions;

class AuthorizationException extends \Exception {

    public function __construct(string $message, int $status) {
        parent::__construct($message, $status);
    }
}
