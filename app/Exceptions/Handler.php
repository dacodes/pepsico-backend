<?php

namespace App\Exceptions;

use App\Exceptions\ApiException;
use App\Exceptions\AuthorizationException as AuthException;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * A list of exception types that should not be rendered
     * 
     * @var array
     */
    protected $dontRender = [
        \App\Exceptions\AuthorizationException::class,
        \App\Exceptions\ApiException::class,
        \App\Exceptions\MailException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {   
        if (!in_array(get_class($exception), $this->dontRender)) {
            return parent::render($request, $exception);
        }

        $status = $exception->getCode();

        return response()->json([
            'status' => $status,
            'error' => $this->getMessage($exception)
        ], $status);
    }

    /**
     * Get response message from request
     * 
     * @param  Exception $exception
     * @return array
     */
    private function getMessage($exception): array 
    {
        if (env('APP_DEBUG')) {
            return [
                'type' => get_class($exception),
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine()
            ];
        }
        
        return ['message' => $exception->getMessage()];
    }
}
