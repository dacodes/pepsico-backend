<?php

namespace App\Providers;

use App\Helpers\MailHelper;
use Illuminate\Support\ServiceProvider;

class MailServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MailHelper::class, function ($app) {
            return new MailHelper(config('mail'));
        });
    }
}
