<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

class FlySystemServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Filesystem::class, function () {
            $adapter = new Local(public_path());
            return new Filesystem($adapter, ['visibility' => 'public']);
        });
    }
}
