<?php

namespace App\Kernel;

use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Application as BaseApp;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\WebProcessor;

/**
 * Kernel app
 */
class App extends BaseApp
{
    function __construct(string $basePath)
    {
        /**
         * Call parent to instantiate the app
         */
        parent::__construct($basePath);

        /**
         * Setup the app
         */
        $this->setup();
    }

    /**
     * Setup the application
     */
    private function setup()
    {
        /**
         * enable facades
         */
        $this->withFacades();

        /**
         * Enable eloquent
         */
        $this->withEloquent();

        /**
         * Load all configurations
         */
        $this->loadConfiguration();

        /**
         * Register app routes
         */
        $this->registerRoutes();

        /**
         * Register providers
         */
        $this->registerProviders();

        /**
         * Register app middlewares
         */
        $this->registerMiddlewares();

        /**
         * Configure logs
         */
        $this->configureLogs();
    }

    /**
     * Collect and inject all configuration files from config folder
     */
    private function loadConfiguration()
    {
        $this->configure('app');

        $directory = array_diff(scandir(config_path()), array('..', '.'));

        collect($directory)->each(function ($item) {
            $routes = require config_path() . '/' . $item;
            $this->make('config')->set(basename($item, '.php'), $routes);
        });
    }

    /**
     * Register the app routes included in the routes configuration
     * @param  array  $routes  array of routes
     */
    private function registerRoutes()
    {
        collect(config('routes'))->each(function ($route) {
            new $route($this->router);
        });
    }

    /**
     * Registers the app service providers included in the providers configuration
     */
    private function registerProviders()
    {
        collect(config('providers'))->each(function ($provider) {
            $this->register($provider);
        });
    }

    /**
     * Registers the app middlewares included in the middlewares configuration
     * @param  array  $middlewares array of middlewares
     */
    private function registerMiddlewares()
    {
        $this->middleware(config('middlewares.global'));

        $routeMiddlewares = config('middlewares.routes');

        if (!empty($routeMiddlewares)) {
            $this->routeMiddleware($routeMiddlewares);
        }
    }

    private function configureLogs()
    {
        $logger = Log::getLogger();
        $logger->pushProcessor(new IntrospectionProcessor());
        $logger->pushProcessor(new WebProcessor());

        foreach (config('log')['handlers'] as $handler) {
            $handler = new RotatingFileHandler($handler['path'], 30, $handler['level']);
            $handler->setFormatter(new JsonFormatter());
            $logger->pushHandler($handler);
        }
    }
}
